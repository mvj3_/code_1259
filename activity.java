package com.google.Android.WaterWave;

import Android.app.Activity;
import Android.os.Bundle;
import Android.view.KeyEvent;
import Android.view.Window;
import Android.view.WindowManager;

public class WaterWave extends Activity
{
 DrawWaterWave m_DrawWaterWave;
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setTheme(Android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
       
        m_DrawWaterWave = new DrawWaterWave(this);
        setContentView(m_DrawWaterWave);
    }
   
 public boolean onKeyDown(int keyCode, KeyEvent event)
 {
  m_DrawWaterWave.key();
  return super.onKeyDown(keyCode, event);
 }
} 